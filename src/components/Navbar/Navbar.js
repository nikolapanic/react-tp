import React from 'react';
import './style.css';

function Navbar() {
    return (
        <div className="Navbar">
            Projet :<span id="proj"> Pokedex</span>
        </div>
    );
}

export default Navbar;
